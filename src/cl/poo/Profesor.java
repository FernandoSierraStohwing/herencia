/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.poo;

/**
 *
 * @author Fernando Sierra
 */
public class Profesor extends Persona{
    private String asignatura;

    public Profesor() {
    }

    public Profesor(String asignatura) {
        this.asignatura = asignatura;
    }

    public Profesor(String asignatura, String nombre, String apellido, int edad) {
        super(nombre, apellido, edad);
        this.asignatura = asignatura;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }
    //para sobre escribir un metodo se escribe la anotacion override
    @Override
    public void mostrar(){
        //para traer la info heredada debemos hacer un super mas el metodo de la superClase
        super.mostrar();
        System.out.println("profesor del ramo de " + this.asignatura);
    }
    
    
}
