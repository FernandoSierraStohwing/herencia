package cl.poo;

public class AlumnoIntercambio extends Alumno{
    private String nacionalidad;

    public AlumnoIntercambio() {
    }

    public AlumnoIntercambio(String institucion, double promedio, String nombre, String apellido, int edad, String nacionalidad) {
        super(institucion, promedio, nombre, apellido, edad);
        this.nacionalidad = nacionalidad;
    }

    @Override
    public void mostrar() {
        super.mostrar();
        System.out.println("nacionalidad: " + this.nacionalidad);
    }
}
