/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.poo;

import cl.poo.Alumno;
import cl.poo.Profesor;

/**
 *
 * @author Fernando Sierra
 */
public class Main {
    public static void main(String[] args) {
        Alumno alumno1 = new Alumno("Duoc", 6.5, "Ana", "Segovia", 18);
        System.out.println("*****Datos alumno*******");
        alumno1.mostrar();
        Profesor profesor1 = new Profesor("Java", "Fernando", "Sierra", 33);
        System.out.println("*****Datos profesor*******");
        profesor1.mostrar();
        System.out.println("************************");
        AlumnoIntercambio alumnoI = new AlumnoIntercambio("Harvard", 3.4, "Boris", "Becker", 18, "Aleman");
        alumnoI.mostrar();

    }
}
