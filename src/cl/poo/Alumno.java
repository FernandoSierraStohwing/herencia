/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.poo;

/**
 *
 * @author Fernando Sierra
 */
public class Alumno extends Persona{
    private String institucion;
    private double promedio;

    public Alumno() {
    }

    public Alumno(String institucion, double promedio, String nombre, String apellido, int edad) {
        super(nombre, apellido, edad);
        this.institucion = institucion;
        this.promedio = promedio;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
    
    //para sobre escribir un metodo se escribe la anotacion override
    @Override
    public void mostrar(){
        //para traer la info heredada debemos hacer un super mas el metodo de la superClase
        super.mostrar();
        System.out.println("institucion: " + this.institucion);
        System.out.println("promedio: " + this.promedio);
    }
    
    
}
